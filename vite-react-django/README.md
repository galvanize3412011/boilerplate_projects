<a name="readme-top"></a>



[![LinkedIn][linkedin-shield]][linkedin-url]



<br />
<div align="center">

  <h3 align="center">Boilerplate Full-Stack Project</h3>

  <p align="center">
    Boilerplate full-stack project with simple setup instructions!
    <br />
    <a href="https://gitlab.com/WayneBasile/boilerplate-vite-django/-/blob/main/README.md"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://gitlab.com/WayneBasile/boilerplate-vite-django/-/blob/main/README.md">View Demo</a>
    ·
    <a href="https://gitlab.com/WayneBasile/boilerplate-vite-django/-/blob/main/.gitlab/issues/bug-report.md">Report Bug</a>
    ·
    <a href="https://gitlab.com/WayneBasile/boilerplate-vite-django/-/blob/main/.gitlab/issues/feature-request.md">Request Feature</a>
  </p>
</div>



<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>
</details>



## About The Project

This project is a quickstart guide to configuring a full-stack web application with Vite, React, Django, Docker, Bootstrap, React-Boostrap, and MUI frontend frameworks.



### Built With

[![Django][Djangoproject.com]][Django-url][![Bootstrap][Bootstrap.com]][Bootstrap-url][![React][React.js]][React-url][![Docker][Docker.com]][Docker-url][![HTML5][HTML5.com]][HTML5-url][![Python][Python.org]][Python-url][![Javascript][Javascript.com]][Javascript-url][![React Router][ReactRouter.com]][ReactRouter-url][![MUI][MUI.com]][MUI-url]

<div align="right">(<a href="#readme-top">back to top</a>)</div>



## Getting Started

The following sections provide detailed instructions for setting up a Vite-React and Django project locally on macOS. Follow these steps to get a local copy up and running on your machine.

### Prerequisites

The installation instructions assume your system has the following software: [a package manager](https://brew.sh/), [Python](https://github.com/pyenv/pyenv#installation), [ipython](https://pypi.org/project/ipython/), [a text editor](https://code.visualstudio.com/), [Google Chrome](https://www.google.com/chrome/), [Git](https://git-scm.com/), and [Gitlab](https://gitlab.com/), and [Docker](https://www.docker.com/). If you don't have this (or an equivalent) software, please install them before proceeding.

### Installation


#### GitLab

Navigate to `https://gitlab.com/projects/new`

Select `Create Blank Project`

Configure Settings and Select `Create Project`

Select `Clone`

Select `Clone with HTTPS`

In Terminal `cd <your_directory>`

In Terminal `git clone https://gitlab.com/<your_username>/<project_name>.git`

In Terminal `cd <project_name>`

In Terminal `code .`


#### Django

In Terminal `python -m venv ./.venv`

In Terminal `source ./.venv/bin/activate`

In Terminal `pip install --upgrade pip`

In Terminal `pip install django`

In Terminal `deactivate`

In Terminal `source ./.venv/bin/activate`

In Terminal `pip freeze > requirements.txt`

In Terminal `mkdir django-backend`

In Terminal `mkdir django-backend/api`

In Terminal `mv requirements.txt django-backend/api`

In Terminal `cd django-backend/api`

In Terminal `django-admin startproject backend_project .`

In Terminal `python manage.py startapp backend_rest`

In settings.py

```python
INSTALLED_APPS = [
	  'backend_rest.apps.BackendRestConfig',
		...
]
```

In Terminal `python manage.py migrate`

In settings.py

```python
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR / 'data/db.sqlite3',
    }
}
```

In Terminal `mkdir data`

In Terminal `mv db.sqlite3 data/`

In Terminal `python manage.py createsuperuser`

In Terminal create username and password, select `y`

In Terminal `python manage.py runserver`

Confirm Install Successful at [`http://localhost:8000/`](http://localhost:8000/)

On Keyboard `CTRL + C`

Create Dockerfile.dev and .gitignore in django-backend/api

In Terminal `docker volume create django-backend-db`

In Terminal `docker build -f Dockerfile.dev . -t django-backend-dev`

In Terminal `docker run -v "$(pwd):/app" -p 8000:8000 django-backend-dev`

Confirm Install Successful at [`http://localhost:8000/`](http://localhost:8000/)

On Keyboard `CTRL + C`

In Terminal `cd ../..`


#### Vite + React

In Terminal `npm create vite@latest`

In Terminal `react-frontend`

Select `React`

Select `Javascript + SWC`

In Terminal `cd react-frontend`

In Terminal `npm install`

In Terminal `npm run dev`

Confirm Install Successful at [`http://localhost:5173/`](http://localhost:5173/)

On Keyboard `CTRL + C`

In vite.config.js

```jsx
export default defineConfig({
  plugins: [react()],
  server: {
    watch: {
      usePolling: true,
    },
    host: true,
    strictPort: true,
    port: 5173,
  }
})
```

Create run.sh and Dockerfile in react-frontend.

In Terminal `docker build -t react-frontend-image .`

In Terminal `docker run -d --rm -p 5173:5173 react-frontend-image`

Confirm Install Successful at [`http://localhost:5173/`](http://localhost:5173/)

On Keyboard `CTRL + C`

In Docker Select `Stop`

In Terminal `cd ..`


#### Docker Compose

Create docker-compose.yml, .gitignore, and .gitattributes in top level directory.

In Terminal `docker compose build`

In Terminal `docker compose up`

Confirm Install Successful at [`http://localhost:8000/`](http://localhost:8000/)

Confirm Install Successful at [`http://localhost:5173/`](http://localhost:5173/)

On Keyboard `CTRL + C`


#### Frontend Toolkit

In Terminal `cd react-frontend`

In Terminal `npm i react-router-dom`

In Terminal `npm i bootstrap@5.3.1`

In Terminal `npm install react-bootstrap bootstrap`

In Terminal `npm install @mui/material @emotion/react @emotion/styled`

In Terminal `npm install @mui/icons-material`

In Terminal `cd ..`


#### Finish!

In Docker `Delete All Containers`

In Docker `Delete All Images`

In Terminal `deactivate`

In Terminal `rm -r .venv`

In Terminal `docker compose build`

In Terminal `docker compose up`

Confirm Install Successful at [`http://localhost:8000/`](http://localhost:8000/)

Confirm Install Successful at [`http://localhost:5173/`](http://localhost:5173/)

On Keyboard `CTRL + C`

#### Importing Compiled CSS

You may use [Bootstrap’s ready-to-use css](https://getbootstrap.com/docs/4.0/getting-started/webpack/#importing-compiled-css) by simply adding this line to your project’s entry point: `import 'bootstrap/dist/css/bootstrap.min.css';`

<div align="right">(<a href="#readme-top">back to top</a>)</div>



## Acknowledgments

* [Shields.io](https://shields.io/)
* [Simple Icons](https://simpleicons.org/)

<div align="right">(<a href="#readme-top">back to top</a>)</div>



[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://www.linkedin.com/in/waynebasile/
[product-screenshot]: images/screenshot.png
[Djangoproject.com]: https://img.shields.io/badge/Django-092E20?style=for-the-badge&logo=django&logoColor=white
[Django-url]: https://www.djangoproject.com/
[React.js]: https://img.shields.io/badge/React-20232A?style=for-the-badge&logo=react&logoColor=61DAFB
[React-url]: https://reactjs.org/
[Bootstrap.com]: https://img.shields.io/badge/Bootstrap-563D7C?style=for-the-badge&logo=bootstrap&logoColor=white
[Bootstrap-url]: https://getbootstrap.com
[Docker.com]: https://img.shields.io/badge/Docker-2496ED?style=for-the-badge&logo=docker&logoColor=61DAFB
[Docker-url]: https://www.docker.com/
[HTML5.com]: https://img.shields.io/badge/HTML5-E34F26?style=for-the-badge&logo=html5&logoColor=white
[HTML5-url]: https://developer.mozilla.org/en-US/docs/Web/HTML
[Python.org]: https://img.shields.io/badge/Python-3776AB?style=for-the-badge&logo=python&logoColor=white
[Python-url]: https://www.python.org/
[Javascript.com]: https://img.shields.io/badge/JavaScript-F7DF1E?style=for-the-badge&logo=javascript&logoColor=white
[Javascript-url]: https://developer.mozilla.org/en-US/docs/Web/JavaScript
[ReactRouter.com]: https://img.shields.io/badge/ReactRouter-CA4245?style=for-the-badge&logo=ReactRouter&logoColor=white
[ReactRouter-url]: https://reactrouter.com/en/main
[MUI.com]: https://img.shields.io/badge/MUI-007FFF?style=for-the-badge&logo=MUI&logoColor=white
[MUI-url]: https://mui.com/
